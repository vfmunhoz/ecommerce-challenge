# Challenge

O projeto foi criado como uma aplicação Spring Boot com Spring Data, base de dados H2 em memória, ActiveMQ como mensageria e maven.
A arquitetura do projeto está bem simples, temos os controllers expondo os endpoints e o repositório para acesso a base. Os testes foram feitos com JUnit e mockito.

Sempre que uma campanha for atualizada é feito o envio de uma mensagem para um tópico no ActiveMQ onde será possível fazer as notificações de que a campanha foi alterada.

Para executar o projeto basta gerar o build e iniciar a JVM.

```bash
mvn clean package
java -jar target/challenge-1.0-SNAPSHOT.jar
```
## Endpoints para testes

### Campaign

(POST) Adiciona Campanha
http://localhost:8080/campanha/add

```javascript
{"campaignName":"Campanha de teste 1","campaignStart":"2018-07-01","campaignEnd":"2018-07-18","team": {"id": 1}}}
```

(GET) Lista todas as campanhas
http://localhost:8080/campanha/

(GET) Lista todas as campanhas de um time
http://localhost:8080/campanha/{team}/listar

(GET) Busca uma campanha por ID
http://localhost:8080/campanha/campanha/{id}

(DELETE) Deleta uma campanha pelo ID
http://localhost:8080/campanha/{id}/deletar

(PUT) Deleta uma campanha pelo ID
http://localhost:8080/campanha/{id}/alterar

```javascript
{"campaignName":"Campanha de teste 1","campaignStart":"2018-07-01","campaignEnd":"2018-07-18","team": {"id": 1}}}
```

### User

(POST) Adiciona usuário
http://localhost:8080/user/add

```javascript
{"name":"Fulano de Tal","email":"fulano2@fulano.com","birthdate":"2018-07-18", "team": {"id": 1}}
```

(PUT) Faz o subscribe de um usuário a uma campnha 
http://localhost:8080/user/{userID}/subscribe/{campaignID}

(GET) Retorna todos os usuários
http://localhost:8080/user

## O que é um deadlock

Um deadlock acontece quando duas ou mais threads comcorrem por um mesmo recurso e a liberação do recurso depende de uma dessas threads, quando isso acontece, como as duas threads entram em estado blocked aguardando pelo mesmo recurso, a execução dessas threads não irão mais acontecer e a partir deste momento podemos dizer que as threads estão em deadlock.

## Uma das grandes inclusões no Java 8 foi a API Stream. Com ela podemos fazer diversas operações de loop, filtros, maps, etc. Porém, existe uma variação bem interessante do Stream que é ParallelStreams. Descreva com suas palavras quando qual é a diferença entre os dois e quando devemos utilizar cada um deles.

Os parallel streams realizam as operações em cima do stream de maneira concorrente, apesar disso o desempenho de um parallel stream nem sempre é maior, o parallel stream precisa de um gerenciamento de concorrência e com isso ele se aplica melhor quando temos operações que podem ser paralelizadas e onde o volume de dados seja realmente significativo.
Caso o volume de dados não seja muito grande ou as operações envolvidas no stream gerem pontos onde o controle é sincronizado (como imprimir os dados do stream) a melhor abordagem é usar um stream comum para evitar a perda de desempenho pelo controle das threads.