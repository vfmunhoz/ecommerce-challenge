package br.com.ecommerce.controller;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import br.com.ecommerce.exception.CampaignNotFoundException;
import br.com.ecommerce.model.Campaign;
import br.com.ecommerce.model.Team;
import br.com.ecommerce.model.User;
import br.com.ecommerce.repository.UserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserControllerTest {

	private static final String TEAM_NAME = "Juventus";
	@MockBean private UserRepository userRepository;
	@MockBean private CampaignController campaignController;
	
	@Autowired private WebApplicationContext webApplicationContext;
	
	private MockMvc mockMvc;
	private User newUser;
	private User savedUser;
	private User savedUserWithCampaign;
	
	private Collection<Campaign> campaigns;
	
	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
		
		Team team = new Team(UserControllerTest.TEAM_NAME);
		
		this.newUser = new User("Usuario de testes", "testes@testes.com", LocalDate.of(1989, Month.OCTOBER, 02));
		this.newUser.setTeam(team);

		this.savedUser = new User("Usuario de testes salvo", "testes@testes.com", LocalDate.of(1989, Month.OCTOBER, 02));
		this.savedUser.setId(1);
		this.savedUser.setTeam(team);
		
		Campaign firstCampaign = new Campaign("Primeira campanha", LocalDate.now(), LocalDate.now().plusDays(2));
		firstCampaign.setTeam(team);
		
		this.campaigns = new ArrayList<>();
		this.campaigns.add(firstCampaign);
		
		this.savedUserWithCampaign = new User("Usuario de testes salvo", "testes@testes.com", LocalDate.of(1989, Month.OCTOBER, 02));
		this.savedUserWithCampaign.setId(1);
		this.savedUserWithCampaign.setTeam(team);
		this.savedUserWithCampaign.setSubscribedCampaigns(this.campaigns);
	}
	
	@Test
	public void testAddUser() throws Exception {
		Mockito.when(this.userRepository.findUserByEmail(this.newUser.getEmail())).thenReturn(null);
		Mockito.when(this.userRepository.save(this.newUser)).thenReturn(this.savedUser);
		
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
		mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
		
		this.mockMvc.perform(post("/user/add")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(this.newUser))
				.accept(MediaType.APPLICATION_JSON))
		.andExpect(status().isCreated())
		.andDo(print());
	}
	
	@Test
	public void testAddExistingUser() throws Exception {
		Mockito.when(this.userRepository.findUserByEmail(this.newUser.getEmail())).thenReturn(this.savedUser);
		Mockito.when(this.campaignController.getAllCampaignsForTeam(this.savedUser.getTeam().getTeamName())).thenReturn(this.campaigns);
		
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
		mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
		
		this.mockMvc.perform(post("/user/add")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(this.newUser))
				.accept(MediaType.APPLICATION_JSON))
		.andExpect(status().isCreated())
		.andExpect(jsonPath("$.availableCampaigns", hasSize(1)))
		.andExpect(jsonPath("$.email").value("testes@testes.com"))
		.andDo(print());
	}
	
	@Test
	public void testSubscribeCampaignToUser() throws Exception {
		Mockito.when(this.userRepository.findById(1)).thenReturn(Optional.of(this.savedUser));
		Mockito.when(this.campaignController.getCampaignByID(1)).thenReturn(this.campaigns.iterator().next());
		Mockito.when(this.userRepository.save(this.savedUser)).thenReturn(this.savedUserWithCampaign);
		
		this.mockMvc.perform(put("/user/1/subscribe/1")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		.andDo(print());
	}
	
	@Test
	public void testSubscribeCampaignNotFoundToUser() throws Exception {
		Mockito.when(this.userRepository.findById(1)).thenReturn(Optional.of(this.savedUser));
		Mockito.when(this.campaignController.getCampaignByID(1)).thenThrow(CampaignNotFoundException.class);
		Mockito.when(this.userRepository.save(this.savedUser)).thenReturn(this.savedUserWithCampaign);
		
		this.mockMvc.perform(put("/user/1/subscribe/1")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
		.andExpect(status().isNotFound())
		.andDo(print());
	}
}
