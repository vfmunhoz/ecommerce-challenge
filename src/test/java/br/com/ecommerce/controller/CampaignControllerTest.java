package br.com.ecommerce.controller;

import static org.assertj.core.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import br.com.ecommerce.model.Campaign;
import br.com.ecommerce.model.Team;
import br.com.ecommerce.repository.CampaignRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CampaignControllerTest {

	private static final String TEAM_NAME = "Juventus";

	@MockBean private CampaignRepository campaignRepository;
	
	@Autowired private CampaignController campaignController;
	@Autowired private WebApplicationContext webApplicationContext;
	
	private MockMvc mockMvc;
	
	private Collection<Campaign> campaignsToUpdate;
	private Campaign newCampaign;
	private Campaign savedCampaign;
	
	@Before
	public void setup() {
		Campaign firstCampaign = new Campaign("Primeira campanha", LocalDate.now(), LocalDate.now().plusDays(2));
		Campaign secondCampaign = new Campaign("Segunda campanha", LocalDate.now(), LocalDate.now().plusDays(3));
		
		Team team = new Team(CampaignControllerTest.TEAM_NAME);
		firstCampaign.setTeam(team);
		
		this.campaignsToUpdate = new ArrayList<>();
		this.campaignsToUpdate.add(firstCampaign);
		this.campaignsToUpdate.add(secondCampaign);
		
		this.newCampaign = new Campaign("Nova campanha", LocalDate.now(), LocalDate.now().plusDays(2));
		
		this.savedCampaign = new Campaign("Nova campanha salva", LocalDate.now(), LocalDate.now().plusDays(2));
		this.savedCampaign.setId(1);
		
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
	}
	
	@Test
	public void testSetValidCampaignDate() {
		final Collection<Campaign> updatedCampaigns = this.campaignController.setValidCampaignDate(this.newCampaign, this.campaignsToUpdate);
		
		assertThat(updatedCampaigns.size()).isEqualTo(2);
		assertThat(updatedCampaigns).isInstanceOf(ArrayList.class);
		
		final List<Campaign> campaigns = (List<Campaign>) updatedCampaigns;
		
		assertThat(campaigns.get(0).getCampaignEnd()).isAfter(this.newCampaign.getCampaignEnd());
		assertThat(campaigns.get(0).getCampaignEnd()).isBefore(campaigns.get(1).getCampaignEnd());
	}
	
	@Test
	public void testAddCampaign() throws Exception {
		Mockito.when(this.campaignRepository.save(newCampaign)).thenReturn(this.savedCampaign);
		Mockito.when(this.campaignRepository.findAll()).thenReturn(this.campaignsToUpdate);
		
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
		mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
		
		this.mockMvc.perform(post("/campanha/add/")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(this.newCampaign))
				.accept(MediaType.APPLICATION_JSON))
		.andExpect(status().isCreated())
		.andExpect(jsonPath("$.campaignName").value("Nova campanha salva"));
	}

	@Test
	public void testGetAllCampaigns() throws Exception {
		Mockito.when(this.campaignRepository.findCampaignsByDate(LocalDate.now(ZoneId.of(ZoneId.SHORT_IDS.get("BET")))))
			.thenReturn((List<Campaign>)this.campaignsToUpdate);
		
		this.mockMvc.perform(get("/campanha").accept(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$", hasSize(2)))
		.andDo(print());
	}
	
	@Test
	public void testGetAllCampaignsForTeam() throws Exception {
		Mockito.when(this.campaignRepository.findCampaignsByDate(LocalDate.now(ZoneId.of(ZoneId.SHORT_IDS.get("BET")))))
			.thenReturn((List<Campaign>)this.campaignsToUpdate);
		
		this.mockMvc.perform(get("/campanha/Juventus/listar").accept(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$", hasSize(1)))
		.andExpect(jsonPath("$[0].team.teamName").value(CampaignControllerTest.TEAM_NAME))
		.andDo(print());
	}
}
