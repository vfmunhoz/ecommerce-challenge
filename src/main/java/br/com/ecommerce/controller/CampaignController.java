package br.com.ecommerce.controller;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.ecommerce.exception.CampaignNotFoundException;
import br.com.ecommerce.model.Campaign;
import br.com.ecommerce.repository.CampaignRepository;

@RestController
public class CampaignController {
	
	private static final String CAMPAIGN_UPDATE_TOPIC = "campaign.update.topic";

	private final Logger logger = LoggerFactory.getLogger(CampaignController.class);

	@Autowired
	private CampaignRepository campaignRepository;
	@Autowired
	private JmsTemplate jmsTemplate;

    @GetMapping("/campanha")
    @ResponseBody
    @ResponseStatus(code=HttpStatus.OK)
    public Collection<Campaign> getAllCampaigns() {
    	this.logger.info("Retrieving all campaigns with end date up to =["+ LocalDate.now(ZoneId.of(ZoneId.SHORT_IDS.get("BET"))) + "].");
    	
    	return this.campaignRepository.findCampaignsByDate(LocalDate.now(ZoneId.of(ZoneId.SHORT_IDS.get("BET"))));
    }

    @GetMapping("/campanha/{team}/listar")
    @ResponseBody
    @ResponseStatus(code=HttpStatus.OK)
    public Collection<Campaign> getAllCampaignsForTeam(@PathVariable(name="team") String team) {
    	this.logger.info("Retrieving all campaigns with end date up to =["+ LocalDate.now(ZoneId.of(ZoneId.SHORT_IDS.get("BET"))) + "] for team [" + team + "].");
    	
    	return this.campaignRepository.findCampaignsByDate(LocalDate.now(ZoneId.of(ZoneId.SHORT_IDS.get("BET")))).parallelStream()
    			.filter((campaign) -> campaign.getTeam() != null)
    			.filter((campaign) -> team.equalsIgnoreCase(campaign.getTeam().getTeamName()))
    			.collect(Collectors.toList());
    }
    
    @GetMapping("/campanha/{id}")
    @ResponseBody
    @ResponseStatus(code=HttpStatus.OK)
    public Campaign getCampaignByID(@PathVariable(name="id") Integer id) throws CampaignNotFoundException {
    	this.logger.info("Retrieving campaign with id=[" + id + "].");
    	Optional<Campaign> campaign = this.campaignRepository.findById(id);

    	if(campaign.isPresent()) {
    		this.logger.debug("Campaign found.");

    		return campaign.get();
    	}
    	
    	this.logger.debug("Campaign not found.");
    	throw new CampaignNotFoundException("Nenhuma campanha foi encontrada para esse id.");
    }

    @PostMapping("/campanha/add")
    @ResponseBody
    @ResponseStatus(code=HttpStatus.CREATED)
    public Campaign addCampaign(@RequestBody Campaign campaign) {
    	this.logger.info("Saving the new campaign=[" + campaign.getCampaignName() + "].");
    	
    	Collection<Campaign> campaigns = this.getAllCampaigns();
    	Collection<Campaign> campaignsToUpdate = this.setValidCampaignDate(campaign, Collections.unmodifiableCollection(campaigns));

    	this.logger.debug("Saving the new campaign=[" + campaign.getCampaignName() + "].");
    	campaignsToUpdate.forEach(campaignToUpdate->this.updateCampaign(campaign.getId(), campaign));

    	return this.campaignRepository.save(campaign);
    }

    @DeleteMapping("/campanha/{id}/deletar")
    @ResponseStatus(code=HttpStatus.NO_CONTENT)
    public void deleteCampaign(@PathVariable(name="id") Integer id) {
    	this.logger.info("Deleting campaign=[" + id + "]");

    	this.campaignRepository.deleteById(id);
    }

    @PutMapping("/campanha/{id}/alterar")
    @ResponseBody
    @ResponseStatus(code=HttpStatus.OK)
    public Campaign updateCampaign(@PathVariable(name="id") Integer id, @RequestBody Campaign campaign) {
    	this.logger.info("Updating campaign=[" + id + "]");

    	Campaign savedCampaign = this.campaignRepository.save(campaign);
    	
    	this.sendCampaignUpdateNotification(savedCampaign);

    	return savedCampaign;
    }

	/**
	 * Change the end date of the existing campaigns
	 * @param newCampaign
	 * @param campaigns
	 * @return
	 */
	protected Collection<Campaign> setValidCampaignDate(final Campaign newCampaign, final Collection<Campaign> campaigns) {
		this.logger.info("Filtering the campaigns that may require an update.");
		
		List<Campaign> campaignsToUpdate = campaigns.parallelStream()
			.filter((campaign) -> newCampaign.getCampaignStart().isBefore(((Campaign)campaign).getCampaignEnd()))
			.sorted((firstCampaign, secondCampaign) -> firstCampaign.getCampaignEnd().compareTo(secondCampaign.getCampaignEnd()))
			.peek((campaign) -> campaign.setCampaignEnd(campaign.getCampaignEnd().plusDays(1)))
			.collect(Collectors.toList());
		
		this.logger.debug("Number of campaigns that may be changed=[" + campaignsToUpdate.size() + "].");
		
		LocalDate earliestCampaignDate = newCampaign.getCampaignEnd();

		for(Campaign currentCampaign : campaignsToUpdate) {
			if(!earliestCampaignDate.isEqual(currentCampaign.getCampaignEnd())) continue;
			
			earliestCampaignDate = currentCampaign.getCampaignEnd().plusDays(1);
			currentCampaign.setCampaignEnd(earliestCampaignDate);
			
			this.logger.debug("Campaign=[" + currentCampaign.getId() + "] changed to date=[" + earliestCampaignDate + "].");
		}

		return campaignsToUpdate;
	}
	
	/**
	 * Notifies about a campaign update.
	 * 
	 * @param savedCampaign
	 */
	private void sendCampaignUpdateNotification(final Campaign savedCampaign) {
		try {
			this.jmsTemplate.convertAndSend(CampaignController.CAMPAIGN_UPDATE_TOPIC, savedCampaign);
		} catch (JmsException e) {
			this.logger.info("Was not possible to send the jms message about the campaign update.");
			this.logger.error(e.getMessage());
		}
	}
}
