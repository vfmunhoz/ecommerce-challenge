package br.com.ecommerce.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.ecommerce.exception.CampaignNotFoundException;
import br.com.ecommerce.exception.UserException;
import br.com.ecommerce.model.Campaign;
import br.com.ecommerce.model.User;
import br.com.ecommerce.repository.UserRepository;

@RestController
public class UserController {

	private final Logger logger = LoggerFactory.getLogger(CampaignController.class);
	
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private CampaignController campaignController;

	@PostMapping("/user/add")
    @ResponseBody
    @ResponseStatus(code=HttpStatus.CREATED)
    public User addUser(@RequestBody User user) {
    	this.logger.info("Saving the new user=[" + user.getName() + "].");
    	User userSaved = this.userRepository.findUserByEmail(user.getEmail());
    	
    	if(userSaved != null) {
    		this.logger.info("User=[" + user.getName() + "] already exists.");
    		
    		Collection<Campaign> campaigns = this.campaignController.getAllCampaignsForTeam(userSaved.getTeam().getTeamName());;
    		userSaved.setAvailableCampaigns(campaigns);
    		
    		this.logger.info("Found [" + campaigns.size() + "] campaigns suitable for the user.");
    	} else {
    		userSaved = this.userRepository.save(user);
    		
    		this.logger.info("User saved successfully.");
    	}

    	return userSaved;
    }

	@PutMapping("/user/{userID}/subscribe/{campaignID}")
	@ResponseStatus(code=HttpStatus.OK)
	public void subscribeCampaignToUser(@PathVariable(name="userID") Integer userID, @PathVariable(name="campaignID") Integer campaignID) throws UserException {
		Optional<User> userOptional = this.userRepository.findById(userID);
		Optional<Campaign> campaignOptional = this.findCampaignById(campaignID);
		
		this.logger.info("Checking if the user and the campaign already exists.");
		if(userOptional.isPresent() && campaignOptional.isPresent()) {
			User user = userOptional.get();
			user.addCampaign(campaignOptional.get());
			
			this.logger.info("Subscribing the user to the campaign");
			this.userRepository.save(user);
		} else {
			this.logger.error("User or campaign invalid.");
			throw new UserException("User or campaign couldnt be found.");
		}
	}
	
	@GetMapping("/user")
	@ResponseBody
	@ResponseStatus(code=HttpStatus.OK)
	public Collection<User> listUsers() {
		Collection<User> usuarios = new ArrayList<>();
		this.userRepository.findAll().forEach(usuarios::add);

		return usuarios;
	}

	/**
	 * Finds a campaign by its id
	 * 
	 * @param campaignID
	 * @return
	 */
	private Optional<Campaign> findCampaignById(Integer campaignID) {
		Optional<Campaign> campaign = null;
		
		try {
			campaign = Optional.of(this.campaignController.getCampaignByID(campaignID));
		} catch (CampaignNotFoundException e) {
			this.logger.info("Campaign not found.");
			campaign = Optional.ofNullable(null);
		}
		
		return campaign;
	}
}
