package br.com.ecommerce.controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jms.annotation.EnableJms;

@SpringBootApplication
@EnableAutoConfiguration
@EntityScan(basePackages = {
	"br.com.ecommerce.model"
})
@EnableJpaRepositories(basePackages = {
	"br.com.ecommerce.repository"
})
@EnableJms
public class Application {

    public static void main(String... args) {
        SpringApplication.run(Application.class, args);
    }
}
