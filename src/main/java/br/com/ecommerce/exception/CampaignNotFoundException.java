package br.com.ecommerce.exception;

import java.io.Serializable;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="Campaign not found.")
public class CampaignNotFoundException extends Exception implements Serializable {

	private static final long serialVersionUID = 3302967005473300735L;

	public CampaignNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public CampaignNotFoundException(String message) {
		super(message);
	}

	public CampaignNotFoundException(Throwable cause) {
		super(cause);
	}

	 
}
