package br.com.ecommerce.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.ecommerce.model.Campaign;
import br.com.ecommerce.model.Team;

@Repository
public interface CampaignRepository extends CrudRepository<Campaign, Integer> {

	@Query("SELECT c FROM Campaign c where c.campaignEnd >= :validDate")
	public List<Campaign> findCampaignsByDate(@Param("validDate") LocalDate validDate);
	public List<Campaign> findCampaignsByTeam(Team team);
	
}
