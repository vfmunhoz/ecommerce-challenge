package br.com.ecommerce.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.ecommerce.model.User;

public interface UserRepository extends CrudRepository<User, Integer> {

	public User findUserByEmail(String email);
}
