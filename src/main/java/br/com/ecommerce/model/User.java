package br.com.ecommerce.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity
@Table(name="fan_user")
@JsonInclude(Include.NON_NULL)
public class User implements Serializable {

	private static final long serialVersionUID = -4146801457836110907L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;
	private String name;
	@Column(unique=true)
	private String email;
	private LocalDate birthdate;
	@ManyToOne(cascade={CascadeType.REFRESH, CascadeType.DETACH})
    private Team team;
	@OneToMany(cascade={CascadeType.REFRESH, CascadeType.DETACH})
	private Collection<Campaign> subscribedCampaigns;
	@Transient
	private Collection<Campaign> availableCampaigns;
	
	public User() {
	}

	public User(String name, String email, LocalDate birthdate) {
		this();
		this.name = name;
		this.email = email;
		this.birthdate = birthdate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDate getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(LocalDate birthdate) {
		this.birthdate = birthdate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	public Collection<Campaign> getSubscribedCampaigns() {
		return subscribedCampaigns;
	}

	public void setSubscribedCampaigns(Collection<Campaign> subscribedCampaigns) {
		this.subscribedCampaigns = subscribedCampaigns;
	}

	public Collection<Campaign> getAvailableCampaigns() {
		return availableCampaigns;
	}

	public void setAvailableCampaigns(Collection<Campaign> availableCampaigns) {
		this.availableCampaigns = availableCampaigns;
	}
	
	public void addCampaign(Campaign campaign) {
		if(this.subscribedCampaigns == null) {
			this.subscribedCampaigns = new ArrayList<>();
		}
		
		this.subscribedCampaigns.add(campaign);
	}
}
