package br.com.ecommerce.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity
@Table(name="campaign")
@JsonInclude(Include.NON_NULL)
public class Campaign implements Serializable {

	private static final long serialVersionUID = -3371975159702002704L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;
    private String campaignName;
    private LocalDate campaignStart;
    private LocalDate campaignEnd;
    @ManyToOne(cascade={CascadeType.REFRESH, CascadeType.DETACH})
    private Team team;
    
    public Campaign() {
	}
    
    public Campaign(String campaignName, LocalDate campaignStart, LocalDate campaignEnd) {
    	this.campaignName = campaignName;
    	this.campaignStart = campaignStart;
    	this.campaignEnd = campaignEnd;
    }
    
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getCampaignName() {
		return campaignName;
	}
	
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}
	
	public LocalDate getCampaignStart() {
		return campaignStart;
	}
	
	public void setCampaignStart(LocalDate campaignStart) {
		this.campaignStart = campaignStart;
	}
	
	public LocalDate getCampaignEnd() {
		return campaignEnd;
	}
	
	public void setCampaignEnd(LocalDate campaignEnd) {
		this.campaignEnd = campaignEnd;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((campaignEnd == null) ? 0 : campaignEnd.hashCode());
		result = prime * result + ((campaignName == null) ? 0 : campaignName.hashCode());
		result = prime * result + ((campaignStart == null) ? 0 : campaignStart.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Campaign other = (Campaign) obj;
		if (campaignEnd == null) {
			if (other.campaignEnd != null)
				return false;
		} else if (!campaignEnd.equals(other.campaignEnd))
			return false;
		if (campaignName == null) {
			if (other.campaignName != null)
				return false;
		} else if (!campaignName.equals(other.campaignName))
			return false;
		if (campaignStart == null) {
			if (other.campaignStart != null)
				return false;
		} else if (!campaignStart.equals(other.campaignStart))
			return false;
		return true;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}
}
