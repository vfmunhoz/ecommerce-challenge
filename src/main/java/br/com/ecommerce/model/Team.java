package br.com.ecommerce.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity
@Table(name="team")
@JsonInclude(Include.NON_NULL)
public class Team implements Serializable {

	private static final long serialVersionUID = -7483423743709105075L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;
	@Column(name="team_name")
	private String teamName;
	
	public Team() {
	}
	
	public Team(String teamName) {
		this.teamName = teamName;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
}
